# ansible-train

Тренировка по написанию ansible playbook


В папке лежит Dockerfile с alpine, в котором установлен ssh:
```console
docker build --build-arg PASSWORD=password -t ansible-test/alpine-ssh -f Dockerfile .
docker run -d -p 2223:22 ansible-test/alpine-ssh
docker run -d -p 2222:22 ansible-test/alpine-ssh
```

Это соберет и запустит 2 докер-контейнера, вам надо узнать их IP, это можно сделать через docker inspect conteiner_id

Эти IP надо вставить в файлик hosts.dev.

Затем ввести команду: 
ansible -i hosts -m ping all
Посмотреть на ответы(если ошибка, тогда нужно добавить хосты в known_hosts, просто зайдите ssh root@IP)

Запустить:
ansible-playbook -i hosts.dev start.yml

#Что можно дописать
 - скачивать пакеты не только для alpine
 - bashrc, alias на сложные команды (How to admin)
 - впн
 - запись трафика
 - добавление юзеров
 - выкачка домашней папки


[Документация](https://docs.ansible.com/ansible/2.3/playbooks_intro.html)

[Полезная ссылка раз](https://docs.ansible.com/ansible/2.3/playbooks_conditionals.html "про when")

[Полезная ссылка два](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html "про то как новых юзеров добавлять")
