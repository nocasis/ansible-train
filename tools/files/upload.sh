#!/bin/bash

TMP_FOLDER="/tmp"
TRANSFER_URL="http://data.tsu.ru:8090/"

NAME=`basename "$1"`
FILE_PATH=`readlink -e "$1"`
UPLOAD_FILE_PATH=$FILE_PATH
echo "[$NAME] tar czf"
tar czf $TMP_FOLDER/$NAME.tar.gz $FILE_PATH 2>/dev/null
UPLOAD_FILE_PATH=$TMP_FOLDER/$NAME.tar.gz

echo "[$NAME] Uploading..."
URL=`(curl --upload-file $UPLOAD_FILE_PATH $TRANSFER_URL 2>/dev/null) | tr -d '"'`
echo "$URL"
echo "[$NAME] Uploaded"
rm -f "$UPLOAD_FILE_PATH"
